package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/fatih/color"
	"gitlab.com/EternalWanderer/dice-roller/Coin"
	"gitlab.com/EternalWanderer/dice-roller/Dice"
)

var (
	surfaces, modifier, diceThrows, attacks int
	advantage, disadvantage, coin           bool
)

func main() {
	ParseFlags()

	switch {

	// few basic checks
	case attacks < 1:
		fmt.Println("Attack amount cannot be below 1")
		os.Exit(1)

	case surfaces < 2:
		fmt.Println("Die surfaces cannot be below 2")
		os.Exit(1)

	case diceThrows < 1:
		fmt.Println("Dice thrown cannot be below 1")
		os.Exit(1)

	// die with 2 sides might as well be a coin
	case surfaces == 2 || coin:
		Coin.Toss(diceThrows)

	case advantage:
		result, x_block, y_block := Dice.Advantage()
		if modifier != 0 {
			color.Green("Rolling 1d20 + %d with advantage...\n", modifier)
			color.Yellow("Without modifier...")
			fmt.Printf("\tx: %d\ty: %d\n", x_block, y_block)
			color.Green("\t%d", result)
			color.Yellow("With modifier...")
			fmt.Printf("\tx: %d\ty: %d\n", x_block+modifier, y_block+modifier)
			color.Green("\t%d", result+modifier)
		} else {
			color.Green("Rolling 1d20 with advantage...")
			fmt.Printf("\tx: %d\ty: %d\n", x_block, y_block)
			color.Green("\t%d", result)
		}
		switch result {
		case 20:
			color.Magenta("Natural 20!")
		case 1:
			color.Magenta("Natural 1!")
		}

	case disadvantage:
		result, x_block, y_block := Dice.Disadvantage()
		if modifier != 0 {
			color.Red("Rolling 1d20 + %d with disadvantage...\n", modifier)
			color.Yellow("Without modifier...")
			fmt.Printf("\tx: %d\ty: %d\n", x_block, y_block)
			color.Red("\t%d", result)
			color.Yellow("With modifier...")
			fmt.Printf("\tx: %d\ty: %d\n", x_block+modifier, y_block+modifier)
			color.Red("\t%d", result+modifier)
		} else {
			color.Green("Rolling 1d20 with disadvantage...")
			fmt.Printf("\tx: %d\ty: %d\n", x_block, y_block)
			color.Red("\t%d", result)
		}
		switch result {
		case 20:
			color.Magenta("Natural 20!")
		case 1:
			color.Magenta("Natural 1!")
		}

	case attacks > 1:
		for i := 0; i < attacks; i++ {
			color.Blue("Attack: %d/%d\n", i+1, attacks)
			Dice.Cast(surfaces, diceThrows, modifier)
		}
	default:
		Dice.Cast(surfaces, diceThrows, modifier)
	}
}

func ParseFlags() {
	flag.IntVar(&surfaces, "surfaces", 20, "Use to specify die surfaces, does not apply to advantage and disadvantage")
	flag.IntVar(&surfaces, "s", 20, "Use to specify die surfaces, defaults to 20")

	flag.IntVar(&diceThrows, "throws", 1, "Specify amount of dice to cast")
	flag.IntVar(&diceThrows, "t", 1, "Specify amount of dice to cast")

	flag.IntVar(&modifier, "modifier", 0, "Add modifier to result of rolls")
	flag.IntVar(&modifier, "m", 0, "Add modifier to result of rolls")

	flag.IntVar(&attacks, "attacks", 1, "Roll a set rules multiple times, does not apply to advantage and disadvantage")
	flag.IntVar(&attacks, "a", 1, "Roll a set rules multiple times, does not apply to advantage and disadvantage")

	flag.BoolVar(&advantage, "advantage", false, "Roll with advantage")
	flag.BoolVar(&disadvantage, "disadvantage", false, "Roll with disadvantage")

	flag.BoolVar(&coin, "coin", false, "Toss a coin, same as '-s 2'")

	flag.Parse()
}
