package Dice

import (
	"fmt"
	"math/rand"
	"strings"
	"time"

	"github.com/fatih/color"
)

var (
	surfaces, diceThrows, modidier int
	initRandom                     bool
)

func InitRandom(initialised bool) {
	if initialised {
		return
	}
	rand.Seed(time.Now().Unix())
	initRandom = true
}

func Cast(surfaces, diceThrows, modifier int) {
	InitRandom(initRandom)
	var (
		casts       []int
		cast, total int
	)

	for i := 0; i < diceThrows; i++ {
		cast = rand.Intn(surfaces) + 1
		casts = append(casts, cast)
		total += cast
	}

	switch {
	case modifier != 0:
		color.Yellow("Rolling %dd%d + %d...\n", diceThrows, surfaces, modifier)
	default:
		color.Yellow("Rolling %dd%d...\n", diceThrows, surfaces)
	}

	if diceThrows > 1 {
		color.Magenta("\tIndividual rolls: %s", strings.Trim(fmt.Sprint(casts), "[]"))
	}

	if modifier != 0 {
		fmt.Printf("\tWithout modifier: %d\n", total)
		color.Green("\tWith modifier: %d", total+modifier)
	} else {
		color.Green("%d", total)
	}
}

func SimpleCast() int {
	InitRandom(initRandom)

	var cast = rand.Intn(20) + 1

	return cast
}

func Advantage() (int, int, int) {
	x := SimpleCast()
	y := SimpleCast()
	if x > y {
		return x, x, y
	} else {
		return y, x, y
	}
}

func Disadvantage() (int, int, int) {
	x := SimpleCast()
	y := SimpleCast()
	if x < y {
		return x, x, y
	} else {
		return y, x, y
	}
}
