package Coin

import (
	"fmt"
	"math/rand"
	"strings"
	"time"

	"github.com/fatih/color"
)

var initRandom bool

func InitRandom(initialised bool) {
	if initialised {
		return
	}
	rand.Seed(time.Now().Unix())
	initRandom = true
}

func Toss(castAmount int) {
	InitRandom(initRandom)
	yellow := color.New(color.FgYellow).SprintFunc()
	blue := color.New(color.FgBlue).SprintFunc()
	var (
		coins     []string
		coin      string
		coinState int
	)

	for i := 0; i < castAmount; i++ {
		coinState = rand.Intn(2)
		if coinState == 0 {
			coin = yellow("heads")
		} else {
			coin = blue("tails")
		}
		coins = append(coins, coin)
	}
	if castAmount > 1 {
		color.Yellow("Tossing %d coins...\n", castAmount)
	} else {
		color.Yellow("Tossing coin...\n")
	}
	fmt.Println(strings.Trim(fmt.Sprint(coins), "[]"))
}
