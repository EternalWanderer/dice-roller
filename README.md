# Dice-roller

This is a little dice roller chucked together because I couldn't be bothered to calculate every dicethrow in a rather unbalanced D&D campaign where a single attack could easily deal 5d8 damage every few turns.

## Usage

### Alpine

Presently I only have an Alpine package which can be found in [my repository](https://alpine.voidcruiser.nl).
Instructions on how to add it can be found on the page.

### Compiling

1. Make sure you have at least Go 1.17 installed.
2. Clone this repository `git clone https://gitlab.com/EternalWanderer/dice-roller.git`
3. Move into the directory and type `make install`

### Docker

I also have a docker image containing both this and my [sheet parser](https://gitlab.com/EternalWanderer/dnd-box).
Further instructions on how to use that can be found in it's own repo.
