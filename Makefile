VERSION = 0.4
PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man

SRC = main.go go.mod go.sum Coin Dice

all: dice-roller

dice-roller:
	go build -o dice-roller -trimpath

clean: 
	rm -f dice-roller dice-roller-$(VERSION).tar.gz

dist: clean
	mkdir -p dice-roller-$(VERSION)
	cp -R zsh.completion LICENSE Makefile README.md dice-roller.1 $(SRC) dice-roller-$(VERSION)
	tar -cf dice-roller-$(VERSION).tar dice-roller-$(VERSION)
	gzip dice-roller-$(VERSION).tar
	rm -rf dice-roller-$(VERSION)

install: all
	install -m 0755 -d $(DESTDIR)$(PREFIX)/bin
	install -m 0755 dice-roller $(DESTDIR)$(PREFIX)/bin
	install -m 0755 -d $(DESTDIR)$(MANPREFIX)/man1
	install -m 0644 dice-roller.1 $(DESTDIR)$(MANPREFIX)/man1

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/dice-roller\
		$(DESTDIR)$(MANPREFIX)/man1/dice-roller.1

package: dist
	rsync --progress dice-roller-$(VERSION).tar.gz voidDroplet:/var/www/alpine/src/
	rm -f /var/cache/distfiles/dice-roller*
	abuild checksum
	abuild -r

.PHONY: all dice-roller clean dist install uninstall package
